"""Tests inspecting codecs in a codec queue (configurable fallbacks)."""

import codecs

import pytest

from test_utf_sig import bommed, ascii_compatible, get_file
import test_declared

# add samples that work with the codec queue
samples = {'utf-8': 'sin(β) → 0'}
for enc in bommed:
    samples[enc] = 'logic ← λογοσ'
samples.update(test_declared.samples)


# Encoding `str`:

def test_encode_no_declaration_fallback():
    # Decoding with 'declared' fails if there is no encoding declaration in
    # the data.
    # A fallback can be specified with the ENCODING_QUEUE_SEPARATOR.
    text = 'no\ndeclaration\nGrüße'
    text.encode('declared or utf-8') == text.encode('utf-8')


# Decoding `bytes`:

@pytest.mark.parametrize("encoding", samples.keys())
def test_decode(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    assert data.decode('utf-sig or declared or utf-8') == text


def test_decode_no_bom():
    """Incompatible encodings and data without declaration -> UnicodeError.
    """
    with pytest.raises(UnicodeError):
        print(b'text'.decode('utf-sig or declared'))


@pytest.mark.parametrize("encoding", ascii_compatible)
def test_decode_compatible(encoding):
    """Compatible encodings work if `data` is 7-bit ASCII clean.
    """
    data = 'text!'.encode(encoding)
    assert data.decode('utf-sig or declared or utf-8') == 'text!'


@pytest.mark.parametrize("encoding", ascii_compatible)
def test_decode_error_handler(encoding):
    """Use error-handler.
    """
    data = '«text»'.encode(encoding)
    assert (data.decode('utf-sig or declared or utf-8', errors='replace')
            == '�text�')


# Opening files

@pytest.mark.parametrize("encoding", samples.keys())
def test_open(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    f = get_file(data, encoding='utf-sig or declared or utf-8')
    assert f.read() == text


def test_open_empty():
    f = get_file(b'', 'utf-sig or declared')
    assert f.read() == ''


def test_open_incomplete_bom():
    f = get_file(codecs.BOM_UTF8[:2], encoding='utf-sig or declared')
    with pytest.raises(UnicodeError):
        f.read()


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_size(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    f = get_file(data, 'utf-sig or declared or utf-8')
    assert f.read(1) == text[0]
    assert f.read(2) == text[1:3]
    assert f.read() == text[3:]


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_readlines(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    f = get_file(data, 'utf-sig or declared or utf-8')
    assert f.readlines() == text.splitlines(keepends=True)


@pytest.mark.parametrize("encoding", (*bommed, 'utf-8'))
def test_open_utf_sig_readline(encoding):
    f = get_file('Škoda\nAuto'.encode(encoding), 'utf-sig or utf-8')
    assert f.readline() == 'Škoda\n'
    assert f.readline() == 'Auto'


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_iter(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    f = get_file(data, 'utf-sig or declared or utf-8')
    assert [line for line in f] == text.splitlines(keepends=True)


if __name__ == '__main__':
    pytest.main([__file__])
