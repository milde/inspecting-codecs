"""Tests for the 'utf-sig' codec."""

import codecs
import tempfile

import pytest

from inspecting_codecs import utf_sig

# Encodings that can be detected by codec 'utf-sig'.
bommed = ('utf-8-sig', 'utf-16', 'utf-32')

# Unicode encodings that are not supported by 'utf-sig'.
# These encodings don't add/expect a BOM and are not ASCII compatible.
no_bom = ('utf-8', 'utf-16-le', 'utf-32-le', 'utf-16-be', 'utf-32-be')

# Examples for ASCII-compatible 8-bit encodings
# Work if the data contains only ASCII characters.
ascii_compatible = ('latin1', 'latin5', 'greek', 'cp858')  # and more


def get_file(content, encoding):
    """Return handle to a file containing `content` opened with `encoding`.

    Write the bytes instance `content` to a tmp_path,
    open it with `encoding`, and return the file handle.
    """
    fw = tempfile.NamedTemporaryFile(mode='wb')
    fw.write(content)
    fw.seek(0)
    return open(fw.name, encoding=encoding)


# a fixture factory
# https://docs.pytest.org/en/7.1.x/how-to/fixtures.html#factories-as-fixtures
# used in test_open…() below
@pytest.fixture(params=bommed)
def sample_file(tmp_path, request):
    """Return a fuction to test the `open()` function with 'utf-sig'.

    The returned function encodes its `content` argument with an
    encoding from the sequence `params`,
    writes it to a tmp_path
    and opens it with encoding 'utf-sig' for reading.
    """

    def _sample_file(content):
        # closure: sample is encoded with parameter `encoding`
        encoding = request.param
        path = tmp_path / encoding
        path.write_bytes(content.encode(encoding))
        return open(path, encoding='utf-sig')

    return _sample_file


# The tests
# ---------

def test_get_encoding():
    assert utf_sig.get_encoding('text'.encode('utf-8-sig')) == 'utf-8-sig'
    assert utf_sig.get_encoding('text'.encode('utf-16')) == 'utf-16'
    assert utf_sig.get_encoding('text'.encode('utf-32')) == 'utf-32'
    assert utf_sig.get_encoding(b'text') is None  # definitely no BOM
    # "empty" strings
    assert utf_sig.get_encoding(''.encode('utf-8-sig')) == 'utf-8-sig'
    assert utf_sig.get_encoding(''.encode('utf-32')) == 'utf-32'
    # ambiguous: may be first iteration of BOM_UTF32_LE
    assert utf_sig.get_encoding(codecs.BOM_UTF16_LE) is False
    # ambiguous: no BOM (but BOM may follow in next iteration)
    assert utf_sig.get_encoding(b'') is False


@pytest.mark.parametrize("encoding", bommed)
def test_decode_function(encoding):
    """the decoding function should return (decoded, lenght_consumed)"""
    assert (utf_sig.decode('λογοσ'.encode(encoding))
            == ('λογοσ', len('λογοσ'.encode(encoding))))


def test_decode_function_no_bom():
    """Raise UnicodeError if there is no BOM in the data."""
    with pytest.raises(UnicodeError):
        utf_sig.decode(b'test')


# Use cases

# Encoding `str` (not supported):

def test_encode_method():
    """Encoding is not defined for 'utf-sig'."""
    with pytest.raises(UnicodeError):
        'sample text'.encode('utf-sig')


# Decoding `bytes`:


@pytest.mark.parametrize("encoding", bommed)
def test_decode(encoding):
    """Test `bytes.decode()` with 'utf-sig'.
    """
    assert 'λογοσ'.encode(encoding).decode('utf-sig') == 'λογοσ'


@pytest.mark.parametrize("encoding", (*no_bom, *ascii_compatible))
def test_decode_no_bom(encoding):
    """Incompatible encodings raise a UnicodeError.
    """
    with pytest.raises(UnicodeError):
        'text'.encode(encoding).decode('utf-sig')


@pytest.mark.parametrize("encoding", ascii_compatible)
def test_decode_compatible_with_fallback(encoding):
    """Compatible encodings work if `data` is 7-bit ASCII clean
    and a fallback is given
    """
    assert 'text!'.encode(encoding).decode('utf-sig or ascii') == 'text!'


@pytest.mark.parametrize("encoding", ascii_compatible)
def test_decode_error_handler(encoding):
    """Fallback encodings use error-handler.
    """
    data = '«text»'.encode(encoding).decode('utf-sig or ascii',
                                            errors='replace')
    assert data == '�text�'


# Opening files

def test_open(sample_file):
    f = sample_file('Привет')
    assert f.read() == 'Привет'


def test_open_empty(sample_file):
    f = sample_file('')
    assert f.read() == ''


def test_open_empy_no_bom(tmp_path):
    path = tmp_path / 'incomplete'
    path.write_bytes(b'')
    f = open(path, encoding='utf-sig')
    assert f.read() == ''


@pytest.mark.parametrize("encoding", no_bom)
def test_open_no_bom(encoding):
    f = get_file('Viele\nGrüße'.encode(encoding), 'utf-sig')
    with pytest.raises(UnicodeError):
        f.read()


def test_open_incomplete_bom(tmp_path):
    path = tmp_path / 'incomplete'
    path.write_bytes(codecs.BOM_UTF8[:2])
    f = open(path, encoding='utf-sig')
    with pytest.raises(UnicodeError):
        f.read()


def test_open_size(sample_file):
    f = sample_file('Привет')
    assert f.read(1) == 'П'
    assert f.read(2) == 'ри'
    assert f.read() == 'вет'


def test_open_readlines(sample_file):
    f = sample_file('Viele\nGrüße')
    assert f.readlines() == ['Viele\n', 'Grüße']


def test_open_readline(sample_file):
    f = sample_file('Škoda\nAuto')
    assert f.readline() == 'Škoda\n'
    assert f.readline() == 'Auto'


def test_open_iter(sample_file):
    f = sample_file('Škoda!\nToo bad!\n')
    assert [line for line in f] == ['Škoda!\n', 'Too bad!\n']


if __name__ == '__main__':
    pytest.main([__file__])
