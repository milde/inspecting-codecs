#!/usr/bin/env python3
# :Copyright: © 2022 Günter Milde.
# :License: Released under the terms of the `2-Clause BSD license`_, in short:
#
#    Copying and distribution of this file, with or without modification,
#    are permitted in any medium without royalty provided the copyright
#    notice and this notice are preserved.
#    This file is offered as-is, without any warranty.
#
# .. _2-Clause BSD license: https://opensource.org/licenses/BSD-2-Clause

"""Test the codec queue."""

import codecs

import pytest

import inspecting_codecs
from inspecting_codecs import CodecQueue


def test_getregentry():
    codec_info = codecs.lookup('ascii or latin1')
    assert codec_info.name == 'ascii_or_latin1'


def test_getregentry_invalid():
    # lookup function should return None, so that other functions can kick in
    codec_queue = CodecQueue('ascii_or_utf42')
    assert codec_queue.getregentry() is None
    # if no other package provides this codec, we expect a LookupError:
    with pytest.raises(LookupError):
        codecs.lookup('ascii or utf42')


def test_encode():
    # For *encoding*, utf-8 (rsp. any Unicode encoding) is a catchall
    text = 'Größe'
    data = text.encode('ascii or latin1 or utf-8')
    assert data == text.encode('latin1')
    text2 = 'a→b'
    data = text2.encode('ascii or latin1 or utf-8')
    assert data == text2.encode('utf-8')


def test_encode_invalid():
    with pytest.raises(LookupError) as excinfo:
        'a'.encode('ascii or gibt-es-nicht')
    assert str(excinfo.value) == 'unknown encoding: ascii or gibt-es-nicht'


def test_decode():
    # For *decoding*, latin1 (rsp. any 8-bit encoding) is a catchall
    text2 = 'a→b'
    data = text2.encode('utf-8')
    assert text2 == data.decode('ascii or utf-8 or latin1')
    text = 'Größe'
    data = text.encode('latin1')
    assert text == data.decode('ascii or utf-8 or latin1')


def test_decode_error():
    text = 'Größe'
    data = text.encode('latin1')
    with pytest.raises(UnicodeError) as excinfo:
        data.decode('ascii or utf-16 or utf-8')
    # TODO: check that errors for all encodings are reported
    assert 'utf-8' in str(excinfo.value)


class MyDecoder(inspecting_codecs.InspectingIncrementalDecoder):
    encoding = 'my'


def test_subclass():
    class OtherDecoder(MyDecoder):
        encoding = 'other'
    assert MyDecoder.encoding == 'my'
    other = OtherDecoder()
    assert other.encoding == 'other'


def test_incrementaldecoder():
    codec_info = codecs.lookup('ascii or latin1')
    incrementaldecoder = codec_info.incrementaldecoder()
    assert incrementaldecoder.encoding == 'ascii_or_latin1'
    assert incrementaldecoder.encodings == ['ascii', 'latin1']


def test_open_read(tmp_path):
    text = 'Grüße'
    path = tmp_path/'sample'
    path.write_bytes(text.encode('utf-8'))
    f = open(path, encoding='utf-16 or utf-8')
    assert f.read() == text


def test_open_error(tmp_path):
    text = 'Grüße'
    path = tmp_path/'sample'
    path.write_bytes(text.encode('latin1'))
    f = open(path, encoding='utf-16 or utf-8')
    with pytest.raises(UnicodeError):
        f.read()


if __name__ == '__main__':
    pytest.main([__file__])
