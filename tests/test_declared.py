"""Tests for the 'declared' codec."""

import pytest

from inspecting_codecs import declared
from test_utf_sig import get_file


samples = {'utf-16': '.. -*- coding: utf-16 -*-\n'
                     'sin(β) → 0',
           'utf-16-le': 'title: Why π is wrong\n'
                        'encoding: utf-16-le\n'
                        '\n'
                        'Why τ is right.\n',
           'utf-16-be': '.. -*- coding: utf-16-be -*-\n'
                        'sin(β) → 0',
           'utf-32': '.. -*- coding: utf-32 -*-\n'
                     'logic ← λογοσ\n',
           'latin1': 'Grüße\n'
                     'this is a text in encoding: latin1',
           'latin2': '#!/usr/bin/env python2\n'
                     '# encoding: latin2\n'
                     'greeting = u"cześć"\n',
           'latin4': '# encoding: latin4\n'
                     'škoda',
           'latin10': '# encoding: latin10\n'
                      'škoda',
           'cp1250': 'cześć\n'
                     'we use encoding=cp1250',
           }


# The tests
# =========

@pytest.mark.parametrize("encoding", samples.keys())
def test_get_encoding(encoding):
    assert declared.get_encoding(samples[encoding]) == encoding


def test_get_encoding_missing_declaration():
    # no declaration, more than 2 lines
    assert declared.get_encoding('no\ndeclaration\nhere') is None
    # no declaration, 2 complete lines
    assert declared.get_encoding('no\ndeclaration\n') is None
    # no declaration but less than 2 complete lines
    assert declared.get_encoding('no\ndeclaration') is False


@pytest.mark.parametrize("encoding", samples.keys())
def test_decode_function(encoding):
    """the decoding function should return (decoded, lenght_consumed)"""
    text = samples[encoding]
    data = text.encode(encoding)
    assert declared.decode(data) == (text, len(data))


def test_decode_function_no_declaration():
    with pytest.raises(UnicodeError):
        print(declared.decode(b'text'))


def test_decode_function_empty_input():
    """the decoding function should return (decoded, lenght_consumed)"""
    assert declared.decode(b'') == ('', 0)


# 'declared' codec use cases

# Encoding `str`:

# UTF-8 sample has no declaration
@pytest.mark.parametrize("encoding", samples.keys())
def test_encode(encoding):
    """Encode `str` with declared encoding."""
    text = samples[encoding]
    data = text.encode(encoding)
    assert text.encode('declared') == data


def test_encode_no_declaration():
    """Encoding with declared encoding."""
    with pytest.raises(UnicodeError):
        print('sample text'.encode('declared'))


# Decoding `bytes`:

@pytest.mark.parametrize("encoding", samples.keys())
def test_decode(encoding):
    """Test `bytes.decode()`.
    """
    text = samples[encoding]
    data = text.encode(encoding)
    assert data.decode('declared') == text


# Opening files

# writing

def test_open_write(tmp_path_factory):
    tmpdir = tmp_path_factory.mktemp('open_write')
    text = 'Grüße\nin encoding=latin1\n'
    with open(tmpdir/'good', encoding='declared', mode='w') as f:
        f.write(text)
    with open(tmpdir/'good', mode='rb') as f:
        assert f.read().decode('latin1') == text


def test_open_write_incrementally(tmp_path_factory):
    tmpdir = tmp_path_factory.mktemp('open_write')
    text = 'Grüße\nin encoding=latin1\n'
    lines = text.splitlines(keepends=True)
    with open(tmpdir/'good', encoding='declared', mode='w') as f:
        f.write(lines[0])
        f.write(lines[1])
    with open(tmpdir/'good', mode='rb') as f:
        assert f.read().decode('latin1') == text


def test_open_write_bad(tmp_path):
    """two complete lines but no encoding declaration (missing ":" or "=")"""
    f = open(tmp_path/'writing', encoding='declared', mode='w')
    with pytest.raises(UnicodeError):
        f.write('Grüße\nin encoding latin1\n')


def test_open_write_uncomplete_fine(tmp_path_factory):
    """no error, if the second line has no trailing newline"""
    tmpdir = tmp_path_factory.mktemp('open_write')
    text = 'Grüße\nin encoding=latin1'
    with open(tmpdir/'good', encoding='declared', mode='w') as f:
        f.write(text)
    with open(tmpdir/'good', mode='rb') as f:
        assert f.read().decode('latin1') == text


# def test_open_write_uncomplete_bad(tmp_path):
#     f = open(tmp_path/'writing', encoding='declared', mode='w')
#     text = 'Grüße\n'
#     with pytest.raises(UnicodeError):
#         f.write(text)
#         f.close()  # does not help
#         del(f) # does not help either
#                # - Error thrown in destructor cannot be handled properly
#                # - __del__() is not guaranteed to be called.


def test_open_write_uncomplete_bad_reset(tmp_path):
    """Resetting triggers the expected error."""
    f = open(tmp_path/'writing', encoding='declared', mode='w')
    text = 'Grüße\n'
    with pytest.raises(UnicodeError):
        f.write(text)
        f.seek(0)


# reading

@pytest.mark.parametrize("encoding", samples.keys())
def test_open(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    f = get_file(data, 'declared')
    assert f.read() == text


def test_open_empty():
    f = get_file(b'', 'declared')
    assert f.read() == ''


def test_open_no_declaration():
    text = 'This text\nis missing\n a declaration'
    f = get_file(text.encode('latin1'), 'declared')
    with pytest.raises(UnicodeError):
        print(f.read())


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_readlines(encoding):
    text = samples[encoding]
    lines = text.splitlines(keepends=True)
    data = text.encode(encoding)
    f = get_file(data, 'declared')
    assert f.readlines() == lines


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_readline(encoding):
    # encoding = 'latin1'
    text = samples[encoding]
    lines = text.splitlines(keepends=True)
    data = text.encode(encoding)
    f = get_file(data, 'declared')
    assert f.readline() == lines[0]
    try:
        assert f.readline() == lines[1]
    except IndexError:
        pass  # just one line in text


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_size(encoding):
    text = samples[encoding]
    data = text.encode(encoding)
    f = get_file(data, 'declared')
    assert f.read(1) == text[0]
    assert f.read(2) == text[1:3]
    assert f.read() == text[3:]


@pytest.mark.parametrize("encoding", samples.keys())
def test_open_iter(encoding):
    text = samples[encoding]
    lines = text.splitlines(keepends=True)
    data = text.encode(encoding)
    f = get_file(data, 'declared')
    assert [line for line in f] == lines


if __name__ == '__main__':
    pytest.main([__file__])
