#!/usr/bin/env python3
# :Copyright: © 2022 Günter Milde.
# :License: Released under the terms of the `2-Clause BSD license`_, in short:
#
#    Copying and distribution of this file, with or without modification,
#    are permitted in any medium without royalty provided the copyright
#    notice and this notice are preserved.
#    This file is offered as-is, without any warranty.
#
# .. _2-Clause BSD license: https://opensource.org/licenses/BSD-2-Clause

"""Codecs that detect the encoding of data by inspecting the content.

:utf-sig:   BOM sniffing (no encoding)
:declared:  scan for encoding declaration similar to :PEP:`263`.

Raise UnicodeError, if the encoding cannot be determined.

A fallback can be specified by chaining encoding names separated by " or ",
for example ``encoding="utf-sig or declared or utf-8"`` will try encoding
'utf-sig' first, if this fails try 'declared', and finally 'utf-8'.

The codecs don't support the legacy `codecs.StreamReader`
and `codecs.StreamWriter` interface (cf. :PEP:`400`).
Use standard `open()` instead of `codecs.open()`.

This module is provisional. API and implementation details may change.
"""

# References
#
# HTML Standard § 13.2.3.2 Determining the character encoding
#   https://html.spec.whatwg.org/multipage/parsing.html#determining-the-character-encoding
#
#   * BOM sniffing (first 3 bytes, UTF-32 ignored)
#   * pre-parsing -> `encoding`, `confidence` ("tentative"|"certain")
#
# HTML Standard § 4.2.5 The meta element
#   https://html.spec.whatwg.org/multipage/semantics.html#charset
#
#   The element containing the character encoding declaration must be
#   serialized completely within the first 1024 bytes of the document.

# Other codec-providing packages:
# https://pypi.org/project/ebcdic/
# https://github.com/claudep/translitcodec
# https://pypi.org/project/rot-codec

# Python supports the following stateful codecs:
#
# | cp932 cp949 cp950
# | euc_jis_2004 euc_jisx2003 euc_jp euc_kr
# | gb18030 gbk hz
# | iso2022_jp iso2022_jp_1 iso2022_jp_2 iso2022_jp_2004 iso2022_jp_3
#   iso2022_jp_ext iso2022_kr
# | shift_jis shift_jis_2004 shift_jisx0213
# | utf_8_sig utf_16 utf_32


import codecs

__version_info__ = (0, 1, 0)
__version__ = '.'.join(str(_) for _ in __version_info__)


# Configurable defaults:

ENCODING_DECLARATION_PATTERN = r'coding[:=]\s*([-\w.]+)'
"""Regular expression matching an encoding declaration.

Used for a `re.search()` over the first two lines of the data.
The first group of the match object is the encoding name.
"""

ENCODING_QUEUE_SEPARATOR = '_or_'
"""String separating encodings in a queue.

If `get_codec_info()` is called with a codec name containing the
ENCODING_QUEUE_SEPARATOR, the name is split using the separator and
the resulting encoding names are tried in a queue until one can en-/decode
the data without UnicodeError.

Since Python 3.9, an encoding name is downcased and anything except "[a-z0-9]"
and "." is converted to "_".
"""


# Abstract Base Class:
# With inspiration from the standard "utf8-sig" codec

class InspectingIncrementalDecoder(codecs.BufferedIncrementalDecoder):
    """
    Base class for incremental decoders using encoding indicated in the data.
    """
    encoding = None      # will be determined from data

    def _buffer_decode(self, data, errors, final):
        # Must be implemented in classes inheriting this class
        raise NotImplementedError

    def reset(self):
        """
        Reset the decoder to the initial state.
        """
        self.buffer = b''
        self.encoding = None

    def getstate(self):
        """
        Return the current state of the decoder.

        This must be a (buffered_input, additional_state_info) tuple.
        buffered_input must be a `bytes` object containing bytes that
        were passed to `decode()` that have not yet been converted.
        `additional_state_info` must be a non-negative integer
        representing the state of the decoder WITHOUT yet having
        processed the contents of `buffered_input`.  In the initial state
        and after `reset()`, `getstate()` must return ``(b"", 0)``.

        Here, the second item tells whether the encoding is already
        determined (1) or not (0).
        """
        return (self.buffer, int(bool(self.encoding)))

    def setstate(self, state):
        """
        Set the current state of the encoder.

        `state` must have been returned by `getstate()`.
        """
        self.buffer, known_encoding = state
        if not known_encoding:
            self.encoding = None


class QueueIncrementalDecoder(InspectingIncrementalDecoder):
    """
    Base class for incremental decoders trying a list of codecs.
    """
    encodings = None  # list of encodings, set in subclass
    encoding_index = 0  # currently tried encoding (active encoding)

    def _buffer_decode(self, data, errors, final):

        error = ValueError('No encodings specified (should never happen).')

        if not final:
            # We need all the data before we output anything.
            return ('', 0)  # => try again on the next call
        # TODO: eventually cache intermediate and start output of decoded text
        # when encoding_index == len(encodings) -1
        # -> early output if there is a failure in all but the last encoding.
        for encoding in self.encodings:
            try:
                return codecs.decode(data, encoding, errors), len(data)
            except UnicodeError as e:
                error = e
                continue
        raise error

    def reset(self):
        """
        Reset the decoder to the initial state.
        """
        self.buffer = b''
        self.encoding_index = 0

    def getstate(self):
        """
        Return the current state of the decoder.

        The second item is the index of the currently used encoding.
        """
        return (self.buffer, self.encoding_index)

    def setstate(self, state):
        """
        Set the current state of the encoder.

        `state` must have been returned by `getstate()`.
        """
        self.buffer, self.encoding_index = state


class CodecQueue:
    """The Codec queue: give a sequence of codecs a try.
    """
    def __init__(self, name):
        self.name = name
        self.encodings = name.split(ENCODING_QUEUE_SEPARATOR)

    def encode(self, data, errors='strict'):
        """
        Try encoding `data` with the encodings in `self.encodings`.

        Try encoding `data` with each encoding in succession.
        In case of an UnicodeError, continue with next item.
        """
        error = ValueError('No encodings specified (should never happen).')
        for encoding in self.encodings:
            try:
                return codecs.encode(data, encoding, errors), len(data)
            except UnicodeError as e:
                error = e
                continue
        raise error

    def decode(self, data, errors='strict'):
        """
        Try decoding `data` with the encodings in the `self.encodings`.

        Try decoding `data` with each encoding in succession.
        In case of an UnicodeError, continue with next item.
        """
        error = ValueError('No encodings specified (should never happen).')
        for encoding in self.encodings:
            try:
                return codecs.decode(data, encoding, errors), len(data)
            except UnicodeError as e:
                error = e
                continue
        raise error
        # TODO: collect exceptions, report all?

    def getregentry(self):
        try:
            for enc in self.encodings:
                codecs.lookup(enc)
        except LookupError:
            # if the queue has invalid encodings, we return None, so
            # that other 3rd-party codecs containing the separator still work.
            return None

        class IncrementalDecoder(QueueIncrementalDecoder):
            encoding = self.name
            encodings = self.encodings

        return codecs.CodecInfo(
            name=self.name,
            encode=self.encode,
            decode=self.decode,
            incrementalencoder=NotImplemented,
            incrementaldecoder=IncrementalDecoder,
            streamreader=NotImplemented,
            streamwriter=NotImplemented,
        )


def get_codec_info(encoding):
    """Return CodecInfo for `encoding` or None.
    """
    if ENCODING_QUEUE_SEPARATOR in encoding:
        return CodecQueue(encoding).getregentry()
    if encoding == 'utf_sig':
        from inspecting_codecs import utf_sig
        return utf_sig.getregentry()
    elif encoding == 'declared':
        from inspecting_codecs import declared
        return declared.getregentry()
    return None


codecs.register(get_codec_info)
