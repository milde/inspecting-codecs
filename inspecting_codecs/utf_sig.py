#!/usr/bin/env python3
# :Copyright: © 2022 Günter Milde.
# :License: Released under the terms of the `2-Clause BSD license`_, in short:
#
#    Copying and distribution of this file, with or without modification,
#    are permitted in any medium without royalty provided the copyright
#    notice and this notice are preserved.
#    This file is offered as-is, without any warranty.
#
# .. _2-Clause BSD license: https://opensource.org/licenses/BSD-2-Clause


"""The 'utf-sig' Codec

Works similar to utf-8-sig but also detects utf-16 and utf-32
from the byte order mark (BOM).

Does not support encoding/writing, as the expected encoding is unclear.
"""

import codecs

from . import InspectingIncrementalDecoder


byte_order_marks = {codecs.BOM_UTF32_BE: 'utf-32',  # \x00\x00\xfe\xff
                    codecs.BOM_UTF32_LE: 'utf-32',  # \xff\xfe\x00\x00
                    codecs.BOM_UTF8: 'utf-8-sig',   # \xef\xbb\xbf
                    codecs.BOM_UTF16_BE: 'utf-16',  # \xfe\xff
                    codecs.BOM_UTF16_LE: 'utf-16',  # \xff\xfe
                    }
"""Map BOM to encoding for encoding detection."""


def get_encoding(data):
    """Return the encoding indicated by a byte order mark (BOM).

    If no BOM is found, return None.
    If an incomplete BOM is found, return False.
    """
    # Dict view order guaranteed for Python >= 3.7.
    # Earlier Python version would need sorting by length to
    # prevent labeling of utf-32-le as utf-16-le
    data = bytes(data[:4])  # may be `memoryview`
    for bom, encoding in byte_order_marks.items():
        if data.startswith(bom):
            if len(data) > 2:
                return encoding
    if len(data) < 4 and any(bom.startswith(data)
                             for bom in byte_order_marks):
        # not enough data to discriminate between BOMs
        return False
    return None


# def check_fallback(text):
#     """Raise UnicodeError when data is probably UTF-16 or UTF-32 without BOM.
#
#     Pure 7-bit ASCII text encoded with UTF-16 or UTF-32 can be decoded with
#     'utf-8' codec but result in mojibake with many '\x00' characters.
#     """
#     if '\00' in text[:4] and len(text.replace('\x00', '')) <= len(text)/2:
#         raise UnicodeError(
#             "Encoding seems to be UTF-16 or UTF-32 but no BOM found.")


def encode(data, errors='strict'):
    raise UnicodeError('The "utf-sig" codec does not support encoding.')


def decode(data, errors='strict'):
    """
    Decode `data` with the encoding indicated by a BOM.

    Check `data` for a byte order mark (BOM) and decode with
    'utf-8-sig', 'utf-16', or 'utf-32' according to the find.
    Return decoded data and number of bytes consumed.

    Raise UnicodeError, if no BOM is found (except for empty data).
    """
    if not data:
        return '', 0
    encoding = get_encoding(data)
    if not encoding:
        raise UnicodeError('Cannot determine encoding. No BOM found.')
    return codecs.decode(data, encoding, errors), len(data)


class IncrementalDecoder(InspectingIncrementalDecoder):
    """
    Incremental decoder using the encoding indicated by a BOM.
    """

    def _buffer_decode(self, data, errors, final):
        if not self.encoding:
            encoding = get_encoding(data)
            if encoding is False:
                # not enough data to discriminate between BOMs
                # if not final => try again on the next call
                # if final and `data` is empty or a BOM, output is empty string
                if not final or not data or data in byte_order_marks:
                    return ('', 0)
            if not encoding:
                raise UnicodeError('Cannot determine encoding. No BOM found.')
            self.encoding = encoding
        return data.decode(self.encoding, errors), len(data)


def getregentry():
    return codecs.CodecInfo(
        name='utf-sig',
        encode=encode,
        decode=decode,
        incrementaldecoder=IncrementalDecoder,
        streamreader=NotImplemented,
        streamwriter=NotImplemented,
    )
