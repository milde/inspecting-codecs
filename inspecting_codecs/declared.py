#!/usr/bin/env python3
# :Copyright: © 2022 Günter Milde.
# :License: Released under the terms of the `2-Clause BSD license`_, in short:
#
#    Copying and distribution of this file, with or without modification,
#    are permitted in any medium without royalty provided the copyright
#    notice and this notice are preserved.
#    This file is offered as-is, without any warranty.
#
# .. _2-Clause BSD license: https://opensource.org/licenses/BSD-2-Clause

"""The 'declared' Codec

Determine the encoding by inspecting the data for an encoding declaration
similar to the one used in :PEP:`263`.
If no declaration is found, fall back to UTF-8.

Difference: no restriction to Python comments ('#' not required)
"""

import codecs
import re

from . import ENCODING_DECLARATION_PATTERN, InspectingIncrementalDecoder


def get_encoding(data):
    """Return the encoding indicated by a :PEP:`263`-like declaration.

    If no matching declaration is found in the first 2 lines, return None.
    If data contains less than 3 newlines, return False.
    """
    lines = data.splitlines()
    for line in lines[:2]:
        if not isinstance(line, str):
            line = line.decode('ascii', errors='ignore').replace('\0', '')
        match = re.search(ENCODING_DECLARATION_PATTERN, line)
        if match:
            return match.group(1)
    if len(lines) < 3:
        # incomplete data
        # check for newline on second line
        continuation = b'c' if isinstance(data, bytes) else 'c'
        if len((data+continuation).splitlines()) < 3:
            return False
    return None


def encode(text, errors='strict'):
    """
    Encode `text` with the encoding declared in the first 2 lines.

    Scan the first two lines for a :PEP:`263`-like explicit encoding
    declaration. Return encoded data and length consumed.
    """
    if not text:
        return '', 0
    encoding = get_encoding(text)
    if not encoding:
        raise UnicodeError('No PEP-263-like encoding declaration found in'
                           ' first two lines.`')
    return text.encode(encoding, errors), len(text)


def decode(data, errors='strict'):
    """
    Decode `data` with the encoding declared in the first 2 lines.

    Scan the first two lines for a :PEP:`263`-like explicit encoding
    declaration. Return decoded string and length consumed.
    """
    if not data:
        return '', 0
    data = bytes(data)  # may be `memoryview`
    encoding = get_encoding(data)
    if not encoding:
        raise UnicodeError('No PEP-263-like encoding declaration found in'
                           ' first two lines.`')
    return data.decode(encoding, errors), len(data)


class IncrementalEncoder(codecs.BufferedIncrementalEncoder):
    """Incremental encoder using the encoding declared in the first 2 lines.
    """

    def __init__(self, errors='strict'):
        super().__init__(errors)
        self.encoding = None

    # Raise an error in the destructor??
    # - cannot be properly catched (cf. ../tests/test_declared.py)
    # - __del__ is not guaranteed to run (e.g. with circular references)
    # def __del__(self):
    #     if self.buffer:
    #         raise UnicodeError('No PEP-263-like encoding declaration found'
    #                            ' in first two lines of "{self.buffer}"')

    def _buffer_encode(self, text, errors, final):
        """Incremental encode, determine encoding from declaration in text.

        Warning:
          Possible data loss, if less than 2 newlines are
          written and there is no encoding declaration.
        """
        # According to the `codecs doc`__: "If this is the last call to
        # encode() final must be true (the default is false)."
        #
        # However, open() never calls the IncrementalEncoder with
        # ``final=True``!
        # cf. https://bugs.python.org/issue17404#msg184045
        #
        # __ https://docs.python.org/3/library/codecs.html
        #    #codecs.IncrementalEncoder.encode
        if not text:
            return '', 0
        if not self.encoding:
            self.encoding = get_encoding(text)
            if self.encoding is False and not final:
                # not enough data to parse encoding declaration
                return (b'', 0)  # => try again on the next call
        if not self.encoding:
            raise UnicodeError('No PEP-263-like encoding declaration found in'
                               ' first two lines.`')
        return text.encode(self.encoding, errors), len(text)

    def reset(self):
        # Data in the buffer will be lost: Raise error
        if self.buffer:
            raise UnicodeError('No PEP-263-like encoding declaration found'
                               ' in first two lines of "{self.buffer}"')
        self.buffer = b''
        self.encoding = None

    def getstate(self):
        return (self.buffer, int(bool(self.encoding)))

    def setstate(self, state):
        self.buffer, known_encoding = state
        if not known_encoding:
            self.encoding = None


class IncrementalDecoder(InspectingIncrementalDecoder):
    """Incremental decoder using the encoding declared in the first 2 lines.
    """

    def _buffer_decode(self, data, errors, final):
        """Incremental decode, determine encoding at first call.
        """
        if not data:
            return '', 0
        if not self.encoding:
            self.encoding = get_encoding(data)
            if self.encoding is False and not final:
                # not enough data to parse encoding declaration
                return ('', 0)  # => try again on the next call
        if not self.encoding:
            raise UnicodeError('No PEP-263-like encoding declaration found'
                               ' in first two lines.`')
        return data.decode(self.encoding, self.errors), len(data)


def getregentry():
    return codecs.CodecInfo(
        name='declared',
        encode=encode,
        decode=decode,
        incrementalencoder=IncrementalEncoder,
        incrementaldecoder=IncrementalDecoder,
        streamreader=NotImplemented,
        streamwriter=NotImplemented,
    )
